import argparse
import hashlib

# Parameter Parsing Set Up
parser = argparse.ArgumentParser()
# parser.add_argument('-f', '--file', type=str, help='Run on project level')
parser.add_argument('-3', '--Task3', help='Test output file for task 3', action='store_true')
parser.add_argument('-4', '--Task4', help='Test output file for task 3', action='store_true')
# Store args
args = parser.parse_args()
# print(args)

def testTask3():
    print("Testing the output for Task 3")
    HASHES = [ "bfc42a13564164e136235e66a5d85a23" ]
    for i in range(len(HASHES)):
        _input = input().strip()
        result = hashlib.md5( str.lower(_input).encode() )

        if result.hexdigest() in HASHES:
            HASHES.remove( result.hexdigest() )
        else:
            print("\033[0;31mError: Answer \"" + _input + "\" is incorrect\033[0m")
            exit(1)

    if len(HASHES) == 0:
        print("\033[0;32mSuccess: Your solution was correct! Well done! Please move to the next task\033[0m")
        exit(0)
    else:
        print("\033[0;31mError: Did not provide enough solutions\033[0m")
        exit(1)

def testTask4():
    print("Testing the output for Task 4")
    HASHES = [ "48100dd028934ed1acb1d098a7a19a88", "c9aa40005aaceeb385276752c9b97512" ]
    for i in range(len(HASHES)):
        _input = input().strip()
        result = hashlib.md5( str.lower(_input).encode() )

        if result.hexdigest() in HASHES:
            HASHES.remove( result.hexdigest() )
        else:
            print("\033[0;31mError: Answer \"" + _input + "\" is incorrect\033[0m")
            exit(1)

    if len(HASHES) == 0:
        print("\033[0;32mSuccess: Your solution was correct! Well done! Please move to the next task\033[0m")
        exit(0)
    else:
        print("\033[0;31mError: Did not provide enough solutions\033[0m")
        exit(1)

if (args.Task3):
    testTask3()

elif (args.Task4):
    testTask4()

else:
    print("\033[0;31mError: Please provide the task which you would like to test (--Task3, --Task4, or --Task5)\033[0m")
    exit(1)
