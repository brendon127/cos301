import argparse

# Parameter Parsing Set Up
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--primes', help='Prime Numbers', type=int, nargs='+')

# Store args
args = parser.parse_args()

def determinePrimes(prime):
    n = 0
    m = 0

    print(str(n) + " + " + str(m) + " = " + str(prime))

    # Example output
    # 2 + 3 = 5
    # 2 + 5 = 7

if __name__ == '__main__':
    for p in args.primes:
        determinePrimes(p)